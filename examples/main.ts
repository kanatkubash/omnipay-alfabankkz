import { AlfaKzGateway, AxiosClient, RegisterResponse, OrderStatusResponse } from '../';
import params from './params';
import * as jsMoney from 'js-money';

const a: AlfaKzGateway = new AlfaKzGateway(new AxiosClient);
a.initialize({
	testMode: true,
	userName: params.test.userName,
	password: params.test.password,
});
const transactionId = '18';
const s = a.purchase({
	transactionId,
	amount: 15,
	returnUrl: 'http://goo.gl',
});
(async () => {
	const response = (await s.send() as RegisterResponse);
	console.log(`Succesful: ${response.isSuccesful}`);
	console.log(`Message: ${response.message}`);
	console.log(`URL: ${response.formUrl}`);
	if (!response.isSuccesful) return;
	console.log(`Started performing checks...`);
	const m = a.check({
		transactionId,
		transactionReference: response.transactionReference,
	});
	const checkCallback = async () => {
		const response = (await m.send() as OrderStatusResponse);
		const orderId = response.transactionReference;
		const status = response.transactionStatus;
		const isSuccesful = response.isSuccesful;
		console.log(`Response is succesful: ${isSuccesful}`);
		console.log(`Status of orderId ${orderId} is ${status}`);
	};
	setInterval(checkCallback, 2000);
})();
