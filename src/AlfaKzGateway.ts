import { AbstractRequest, IClient, AbstractGateway, gateway, ReadIObject } from 'omnipay-js';
import RegisterRequest from './message/RegisterRequest';
import OrderStatusRequest from './message/OrderStatusRequest';
import RestNotifyResponse from './message/RestNotifyResponse';
import RegisterResponse from './message/RegisterResponse';

@gateway('alfa-kz')
export default class AlfaKzGateway extends AbstractGateway {
	readonly name: string = 'alfa-kz';
	defaultParameters = {
		pageView: 'DESKTOP',
		currency: 'KZT',
	};
	get userName() {
		return this._parameters['userName'];
	}
	set userName(value: string) {
		this._parameters['userName'] = value;
	}
	get password() {
		return this._parameters['password'];
	}
	set password(value: string) {
		this._parameters['password'] = value;
	}
	get pageView() {
		return this._parameters['pageView'];
	}
	set pageView(value: string) {
		this._parameters['pageView'] = value;
	}
	check(parameters: ReadIObject): OrderStatusRequest {
		return this._createRequest(OrderStatusRequest, parameters);
	}
	purchase(parameters: ReadIObject): RegisterRequest {
		return this._createRequest(RegisterRequest, parameters);
	}
}
