import { IClient, IHttpResponse } from 'omnipay-js';
import axios from 'axios';
import AxiosResponse from './AxiosResponse';
import { stringify } from 'qs';

export default class AxiosClient implements IClient {
	async send(method: 'GET' | 'POST', uri: string, headers: any, body: Object):
		Promise<IHttpResponse> {
		const dataEncoded = this._getEncodedData(headers['Content-Type'], body);
		const result = await axios.post(uri, dataEncoded, headers);
		return new AxiosResponse(result.status, result.data, result.headers);
	}
	async get(uri: string, headers: Object, queryObj: Object): Promise<IHttpResponse> {
		return this.send('GET', uri, headers, queryObj);
	}
	async post(uri: string, headers: Object, postBody: Object): Promise<IHttpResponse> {
		return this.send('POST', uri, headers, postBody);
	}
	protected _getEncodedData(contentType: string, data: Object): any {
		switch (contentType) {
			case 'application/x-www-form-urlencoded':
				return stringify(data);
		}
	}
}
