import { IClient, IHttpResponse, IObject } from 'omnipay-js';

export default class AxiosResponse implements IHttpResponse {
	headers: IObject;
	body: any;
	status: number;
	constructor(status: number, body: any, headers: IObject) {
		this.status = status;
		this.body = body;
		this.headers = headers;
	}
}
