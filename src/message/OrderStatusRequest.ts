import RestRequest from './RestRequest';
import OrderStatusResponse from './OrderStatusResponse';

import { INotification, NotificationStatus, ReadIObject, IObject } from 'omnipay-js';

export default class OrderStatusRequest extends RestRequest {
	protected _createResponse(request: RestRequest, data: IObject): OrderStatusResponse {
		const response = new OrderStatusResponse(request, data);
		response.transactionReference = this.transactionReference;
		response.transactionId = this.transactionId.toString();
		return response;
	}
	protected _path: string = 'getOrderStatus.do';
	get data(): ReadIObject {
		return {
			orderId: this.transactionReference,
		};
	}
}
