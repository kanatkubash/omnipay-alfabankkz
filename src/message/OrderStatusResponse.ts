import RestRequest from './RestRequest';
import { INotification, NotificationStatus, ReadIObject, IObject } from 'omnipay-js';
import RestResponse from './RestResponse';

export default class OrderStatusResponse extends RestResponse implements INotification {
	transactionReference: string;
	transactionId: string;
	get transactionStatus(): NotificationStatus {
		console.log(this._data['OrderStatus']);
		switch (this._data['OrderStatus']) {
			case 0: return NotificationStatus.STATUS_PENDING;
			case 5: return NotificationStatus.STATUS_PENDING;
			case 2: return NotificationStatus.STATUS_COMPLETED;
			default: return NotificationStatus.STATUS_FAILED;
		}
	}
	get message(): string {
		return this._data['ErrorMessage'];
	}
	protected _path: string = 'getOrderStatus.do';
	get data(): ReadIObject {
		return {
			orderId: this.transactionReference,
		};
	}
}
