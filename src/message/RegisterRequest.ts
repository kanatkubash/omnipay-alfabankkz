import RestRequest from './RestRequest';
import RestResponse from './RestResponse';
import RegisterResponse from './RegisterResponse';
import { ReadIObject, IObject } from 'omnipay-js';
// tslint:disable-next-line:import-name
import * as Currencies from 'js-money/lib/currency';

export default class RegisterRequest extends RestRequest {
	get data(): ReadIObject {
		return {
			amount: this.amountInteger,
			orderNumber: this.transactionId,
			currency: Currencies[this.currency].numericCode,
			pageView: this.pageView,
			returnUrl: this.returnUrl,
		};
	}
	protected _path: string = 'register.do';
	protected _createResponse(request: RestRequest, data: IObject = {}): RegisterResponse {
		return new RegisterResponse(request, data);
	}
}
