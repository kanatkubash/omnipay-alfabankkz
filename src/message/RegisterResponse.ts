import RestResponse from './RestResponse';

export default class RegisterResponse extends RestResponse {
	get isSuccesful(): boolean {
		return typeof this.code === 'undefined';
	}
	get transactionReference() {
		return this.isSuccesful ? this._data.orderId : null;
	}
	get message(): string {
		return this._data.errorMessage;
	}
	get code(): string {
		return this._data.errorCode;
	}
	get transactionId(): string {
		return this.isSuccesful ? this._data.orderNumber : null;
	}
	get formUrl(): string {
		return this._data.formUrl;
	}
}
