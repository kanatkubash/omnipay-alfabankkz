import { INotification, NotificationStatus } from 'omnipay-js';

export default class RestNotifyResponse implements INotification {
	transactionReference: string;
	transactionStatus: NotificationStatus;
	message: string;
	public constructor(transactionReference: string) {
		this.transactionReference = transactionReference;
	}
}
