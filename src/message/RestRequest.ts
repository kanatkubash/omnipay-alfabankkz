import { AbstractRequest, IObject, ReadIObject } from 'omnipay-js';
import RestResponse from './RestResponse';

export default abstract class RestRequest extends AbstractRequest {
	requiredFields: string[];
	protected readonly liveEndPoint: string = '';
	protected readonly testEndPoint: string = 'https://web.rbsuat.com/ab/rest';
	protected readonly _contentType: string = 'application/x-www-form-urlencoded';
	protected abstract _path: string;
	get endPoint() {
		return this.testMode ? this.testEndPoint : this.liveEndPoint;
	}
	get baseData(): ReadIObject {
		return {
			userName: this.userName,
			password: this.password,
		};
	}
	protected abstract _createResponse(request: RestRequest, data: IObject): RestResponse;
	async send(): Promise<RestResponse> {
		const data = { ...this.data, ...this.baseData };
		const url = `${this.endPoint}/${this._path}`;
		const result = await this._client.post(url, this._getHeaders(), data);
		return this._createResponse(this, result.body);
	}
	get userName() {
		return this._parameters['userName'];
	}
	set userName(value: string) {
		this._parameters['userName'] = value;
	}
	get password() {
		return this._parameters['password'];
	}
	set password(value: string) {
		this._parameters['password'] = value;
	}
	get pageView() {
		return this._parameters['pageView'];
	}
	set pageView(value: string) {
		this._parameters['pageView'] = value;
	}
	protected _getHeaders(): ReadIObject {
		return {
			'Content-Type': this._contentType,
		};
	}
}
