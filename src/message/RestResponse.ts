import { AbstractResponse, IObject } from 'omnipay-js';

export default abstract class RestResponse extends AbstractResponse {
	get isSuccesful(): boolean {
		// tslint:disable-next-line:triple-equals
		return this.code == '0';
	}
	get code(): string {
		return this._data['ErrorCode'];
	}
	isPending: boolean = false;
	isRedirect: boolean = false;
	isTransparentRedirect: boolean = false;
	isCancelled: boolean = false;
	redirectUrl: string;
	redirectMethod: string;
	redirectData: IObject;
}
